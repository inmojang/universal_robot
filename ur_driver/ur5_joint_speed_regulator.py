#!/usr/bin/env python
import time
# import roslib; roslib.load_manifest('ur_driver')
import rospy
import actionlib
from control_msgs.msg import *
from trajectory_msgs.msg import *
from std_msgs.msg import Float64MultiArray

# For teleoperation
import sys, select, termios, tty 
import numpy as np
import copy

from sensor_msgs.msg import JointState # To receive the current state of UR

reload(sys)  # to enable `setdefaultencoding` again
sys.setdefaultencoding("UTF-8")

global joint_speed_local
joint_speed_local = None


# For Debugging: Should be deleted when committed
reload(sys)  # to enable `setdefaultencoding` again
sys.setdefaultencoding("UTF-8")

JOINT_NAMES = ['shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint',
               'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']

############ User Setting ##############
num_filter_stack = 20
publish_rate = 250
decaying_rate = 0.96
########################################


def cb_joint_speed(status):
    global joint_speed_local
    joint_speed_local = status

def joint_speed_regulator(pub):
    global joint_speed_local
    
    
    rate = rospy.Rate(publish_rate)

    ## ur_bringup
    g = JointTrajectory()
    g.joint_names = JOINT_NAMES

    # ros_control
    # g = Float64MultiArray()

    vel_0 = np.array([0,0,0,0,0,0])
    vel_joint_stack = [vel_0]*num_filter_stack # Initalise the stack for filtering the desired joint velocity

    print("Waiting for ROS Time")
    while rospy.get_time() == 0: # Waiting for receiving the first message from /clock
        pass    
    time_now = rospy.get_rostime() 
    print("Got ROS Time")

    print("Waiting 'joint_speed_' signal \n")
    while joint_speed_local is None:        
        pass
    print("Connected \n")


    while not rospy.is_shutdown(): 


        joint_speed_status = copy.deepcopy(joint_speed_local)
        time_now_in_conrol = joint_speed_status.header.stamp

        latency = (time_now - time_now_in_conrol).to_sec() 
        if latency > 0.2:
            print("Latency: " + str(latency) + "\n")
            vel_joint_mean = vel_joint_mean*decaying_rate
            
            ## ur_bringup
            g.points = [JointTrajectoryPoint(velocities=[0,0,0,0,0,0])]
            
            ## ros_control
            # g.data = [0,0,0,0,0,0]
            vel_joint_stack = [vel_0]*num_filter_stack

        else:
            # Filter            
            velocities_now = joint_speed_status.points[0].velocities
            
            vel_joint_stack.pop(0) 
            vel_joint_stack.append(velocities_now)
            vel_joint_mean = np.mean(vel_joint_stack, axis = 0)

        time_now = rospy.get_rostime()
        ## ur5_bringup
        g.header.stamp = time_now        
        g.points = [JointTrajectoryPoint(velocities=vel_joint_mean)]
        
        ## ros_control
        # g.data = vel_joint_mean
            
                

           
        pub.publish(g)
        rate.sleep()


def main():
 
    try:
        rospy.init_node("joint_speed_regulator", anonymous=True, disable_signals=True)        
        pub = rospy.Publisher('/ur_driver/joint_speed',JointTrajectory, queue_size=10)       
        # pub = rospy.Publisher('/joint_group_vel_controller/command',Float64MultiArray, queue_size=10)       

        # Subscribing information from Unity
        rospy.Subscriber('/ur_driver/joint_speed_',JointTrajectory, cb_joint_speed, queue_size=10)  
        joint_speed_regulator(pub)

    except KeyboardInterrupt:
        rospy.signal_shutdown("KeyboardInterrupt")
        raise

if __name__ == '__main__': 
    main()
