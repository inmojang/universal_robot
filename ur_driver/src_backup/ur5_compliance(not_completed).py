#!/usr/bin/env python
import time
# import roslib; roslib.load_manifest('ur_driver')
import rospy
import actionlib
from control_msgs.msg import *
from trajectory_msgs.msg import *

# For teleoperation
import sys, select, termios, tty 
import numpy as np

from rain_unity.msg import rain_system  as RainMsg # To receive the system status from Unity
from sensor_msgs.msg import JointState # To receive the current state of UR
# from leap_motion.msg import Human  as LeapMsg # To receive the info from LEAP
from rain_unity.msg import Human_orion  as LeapMsg # To receive the info from LEAP
from geometry_msgs.msg import *



# For forward/inverse kinematics
from ur_kinematics import Kinematics
import math
import copy
###############################################################
###############################################################
Trigger_grab_strength = 0.6 # Set the value in 0 to 1. 
Movement_scale_factor_realrobot = 0.001  # Scaling the joint velocity input (For Real)

num_filter_stack = 1
num_wrench_stack = 100
vel_safety_bound = 4*math.pi # (rad/s) Prevent movement above this value
publish_rate = 10000   
###############################################################
###############################################################
###############################################################
###############################################################


global palm_centre, LeapMsg_local, ur_status_local, pub, teleoperation_mode, wrench_stack
ur_status_local = None
ur_wrench_local = None
teleoperation_mode = ""
LeapMsg_local = None
kin = Kinematics('ur5')

Q0 = [-0.12694727059672406, -1.331667696607827, 2.391941365528808, -1.1109140138393911, 1.545242764007165, 0.13237981553654432]

client = None

JOINT_NAMES = ['shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint',
               'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']

wrench_stack = [np.array([0,0,0,0,0,0])]*num_wrench_stack

def cb_teleop_mode(status):
    global teleoperation_mode
    teleoperation_mode = status.teleoperation_mode

def cb_Leap(status):
    global LeapMsg_local
    LeapMsg_local = status

def cb_UR(status):
    global ur_status_local
    ur_status_local = status

def cb_Wrench(status):
    global ur_wrench_local, wrench_stack
    wrench_stack.pop(0)
    wrench_now =  [status.wrench.force.x, status.wrench.force.y, status.wrench.force.z, 0, 0, 0]
    wrench_stack.append(wrench_now) 
    ur_wrench_local = np.mean(wrench_stack, axis = 0)


def get_ee_frame(ur_status):


    pose_current_ = np.array(ur_status.position) # For Real

    
    pose_current = kin.inverse(kin.forward(pose_current_),pose_current_) # To avoid the case, IK(FK(pose_current)) != pose_current, which is wei
    # To avoid joint angles > pi, which may cause large del_joint afterwards
    for i in range(len(pose_current)):
        if pose_current[i] > math.pi:
            pose_current[i] -= 2*math.pi
        elif pose_current[i] < -math.pi:
            pose_current[i] += 2*math.pi

    q_guess = pose_current               # Use current joint angles asa guess value for IK
    pose_current_xyz = kin.forward(pose_current) # Convert the currentjoints status to x
    # Debug 1
    Dummy = pose_current - kin.inverse(pose_current_xyz, q_guess)
    for i in range(len(Dummy)):
        if np.abs(Dummy[i]) > 0.000001:
            while not rospy.is_shutdown():
                print("Problem with IK")
    
    ee_frame = copy.deepcopy(pose_current_xyz[0:3,0:3])
    return [ee_frame, pose_current, pose_current_xyz]


def palm_velocity_to_joint_velocity(pub):
    global LeapMsg_local, ur_status_local, teleoperation_mode
   
    kin = Kinematics('ur5')
    
    g = JointTrajectory()
    g.joint_names = JOINT_NAMES

    rate = rospy.Rate(publish_rate)

    time_to_reach = 0.008 # This value doesn't affect much
   

    scale_factor = Movement_scale_factor_realrobot


    vel_0 = np.array([0,0,0,0,0,0])
    vel_joint_stack = [vel_0]*num_filter_stack # Initalise the stack for filtering the desired joint velocity

    print("Waiting for ROS Time")
    while rospy.get_time() == 0: # Waiting for receiving the first message from /clock
        pass    
    time_now = rospy.get_rostime() 
    print("Got ROS Time")


    print("Waiting for Robot states")
    while ur_status_local is None: # Wait until UR is connected
        pass
    ur_status = copy.deepcopy(ur_status_local)
    print("Connected to Robot states")

    print("Waiting for Robot Wrench")
    while ur_wrench_local is None: # Wait until UR is connected
        pass
    ur_wrench = copy.deepcopy(ur_wrench_local)
    print("Connected to Robot Wrench")



    while not rospy.is_shutdown():


        # Get Current Status
        ur_wrench = copy.deepcopy(ur_wrench_local)
        ur_status = copy.deepcopy(ur_status_local)
        time_now = rospy.get_rostime()           

        del_x = -ur_wrench[0]*time_to_reach*scale_factor
        del_y = -ur_wrench[1]*time_to_reach*scale_factor
        del_z = ur_wrench[2]*time_to_reach*scale_factor

        del_xyz = np.matrix([[0,0,0,del_x],[0,0,0,del_y],[0,0,0,del_z],[0,0,0,0]])

        ee_frame, pose_current, pose_current_xyz = get_ee_frame(ur_status)

        pose_desired_xyz = np.add(pose_current_xyz, del_xyz)
        weights_joints_kin = [10, 10, 10, 1, 1, 1]
        q_guess =pose_current
        pose_desired = kin.inverse(pose_desired_xyz,q_guess,weights=weights_joints_kin)   
        if(pose_desired is None):
            current_time = time.clock()
            print("[!!!Inverse Kinematics Error!!!]: Please move the robot in a different direction " + str(current_time))
            vel_joint = vel_0
            while not rospy.is_shutdown():
                print("Stop for safety reason!!!: IK Issue \n")          
        else:
            del_joint = pose_desired - pose_current
            for i in range(len(pose_desired)):
                if del_joint[i] > math.pi:
                    del_joint[i] -= 2*math.pi
                elif del_joint[i] < -math.pi:
                    del_joint[i] += 2*math.pi
                    
            vel_joint = (del_joint)/time_to_reach
        # Filter
        vel_joint_stack.pop(0) 
        vel_joint_stack.append(vel_joint) 
        vel_joint_mean = np.mean(vel_joint_stack, axis = 0)
        # Debug
        if np.abs(vel_joint_mean.max()) > vel_safety_bound or np.abs(vel_joint_mean.min()) > vel_safety_bound:
            while not rospy.is_shutdown():
                print("Stop for safety reason: Velocity input is too high!!!")
        
        # Send the target position info
        output_vel_joint = vel_joint_mean
                            
        
        g.header.stamp = time_now 
        g.points = [JointTrajectoryPoint(velocities=output_vel_joint)]
        pub.publish(g)
        rate.sleep()






def main():

    try:
        rospy.init_node("ur_control_mode0",anonymous=True,  disable_signals=True)
        pub = rospy.Publisher('/ur_driver/joint_speed_',JointTrajectory, queue_size=1)
        # Subscribing information
        rospy.Subscriber("/rain/status",RainMsg, cb_teleop_mode, queue_size=1)          
        rospy.Subscriber("/rain/leap_motion",LeapMsg, cb_Leap, queue_size=1)
        rospy.Subscriber("/wrench",WrenchStamped, cb_Wrench, queue_size=1)
        # Subscribing information from UR5 (You need to select one of the belows)


        rospy.Subscriber("/joint_states",JointState, cb_UR, queue_size=1) # For real robot
        
        palm_velocity_to_joint_velocity(pub)
   
    except KeyboardInterrupt:
        rospy.signal_shutdown("KeyboardInterrupt")
        raise


        
    

if __name__ == '__main__': 
    
    main()
     