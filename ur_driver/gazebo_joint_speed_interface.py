#!/usr/bin/env python
import time
import rospy
import actionlib
from control_msgs.msg import *
from trajectory_msgs.msg import *
from sensor_msgs.msg import JointState # To receive the current state of UR

import threading
import numpy as np


client = None
joint_speed_local = None


JOINT_NAMES = ['shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint',
               'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']




################# User Settings ################
maximum_waittime = 1 # (sec)
Q0 = [0,0,0,0,0,0] 
Q1 = [-0.12694727059672406, -1.331667696607827, 2.391941365528808, -1.1109140138393911, 1.545242764007165, 0.13237981553654432] # Initial Position
pub_rate = 250 # Hz
################################################

            

def cb_joint_speed(status):
    global joint_speed_local
    joint_speed_local = status

def cb_UR_gazebo(status):
    global joint_state_local

    joint_state_local.header = status.header
    joint_state_local.position = status.actual.positions
    joint_state_local.velocity = status.actual.velocities

def robot_config_init(): 
    print "Initialisaing the robot's configuration"

    g = FollowJointTrajectoryGoal()
    g.trajectory = JointTrajectory()
    g.trajectory.joint_names = JOINT_NAMES

    # Initilise UR5 position (to the predefined position, Q0)
    pose = Q0
    g.trajectory.points = [
        JointTrajectoryPoint(positions=pose, velocities=[0]*6, time_from_start=rospy.Duration(0.1))]
    client.send_goal(g)
    try:
        client.wait_for_result()
    except KeyboardInterrupt:
        client.cancel_goal()
        raise    

    time.sleep(2)

    # Then to the predefined position, Q1)
    pose = Q1
    g.trajectory.points = [
        JointTrajectoryPoint(positions=pose, velocities=[0]*6, time_from_start=rospy.Duration(0.04))]
    client.send_goal(g)
    try:
        client.wait_for_result()
    except KeyboardInterrupt:
        client.cancel_goal()
        raise  

    print "Initialisation completed"


def control_gazebo_ur():

    global client
    global joint_speed_local
    
    # Action Goal
    g = FollowJointTrajectoryGoal()
    g.trajectory = JointTrajectory()
    g.trajectory.joint_names = JOINT_NAMES

    time_to_reach = 0.1

    num_stack = 2 # This is to get time_pre
    time_info_stack = [0]*num_stack 
    pose = Q1

    while not rospy.is_shutdown():

        time_now = rospy.get_time() 

        if joint_speed_local is not None:

            joint_speed_now = joint_speed_local
            
            

            time_info_stack.pop(0) 
            time_info_stack.append(time_now)
            time_pre = np.amin(time_info_stack)
            time_now = np.amax(time_info_stack) 

            if (time_now - time_pre) > 0 and (time_now - time_pre) < maximum_waittime:

                

                desired_joint_speed = joint_speed_now.points[0].velocities   

                time_to_reach = time_now - time_pre
                del_pose = np.dot(time_to_reach,desired_joint_speed)
                pose = np.add(pose,del_pose)
                    

                g.trajectory.points = [
                    JointTrajectoryPoint(positions=pose.tolist(), effort=[10]*6, time_from_start=rospy.Duration(0.04))]
                client.send_goal(g)
                try:
                    client.wait_for_result()
                except KeyboardInterrupt:
                    client.cancel_goal()
                    raise
            elif (time_now - time_pre) > 1:
                print("Far-away sequential signals at " + str(time_now) + " \n")
        else:
            print("No Signal at " + str(time_now) + "\n")

# Thread class for publishing Gazebo UR joint states
class pub_joint_state (threading.Thread):
    def __init__(self,threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
        self.pub = rospy.Publisher('/joint_states_gazebo',JointState,queue_size=1)

    def run(self):
        global joint_state_local
        joint_state_local = JointState()
        joint_state_local.name = JOINT_NAMES        
        rate = rospy.Rate(pub_rate)
        while not rospy.is_shutdown():
            joint_state_now = joint_state_local
            self.pub.publish(joint_state_now)
            rate.sleep()





def main():
    global client, pub, joint_state_local
 
    try:
        rospy.init_node("GazeboUR_interface", anonymous=True, disable_signals=True)        
        # Action Client to control the Gazebo UR
        client = actionlib.SimpleActionClient('/arm_controller/follow_joint_trajectory', FollowJointTrajectoryAction)
        print "Waiting for Gazebo UR Robot"
        client.wait_for_server()
        print "Connected to Gazebo UR Robot"

        # Initilise the robot configuration
        robot_config_init()
        
        # Subscribing Gazebo UR states and publish a reorganised topic (as a thread)
        # (As the existing Gazebo's joint_states is not the same as the real robot's topic, the following should be included.)
        rospy.Subscriber("/arm_controller/state",JointTrajectoryControllerState, cb_UR_gazebo, queue_size=1)
        pub_joint_state_thread = pub_joint_state(1, "Thread-1",1)
        pub_joint_state_thread.start()
        

        # Subscribing joint_speed input and give the corresponding command to the Gazebo robot
        rospy.Subscriber("/ur_driver/joint_speed",JointTrajectory, cb_joint_speed, queue_size=1)
        control_gazebo_ur()

    except KeyboardInterrupt:
        rospy.signal_shutdown("KeyboardInterrupt")
        raise

if __name__ == '__main__': 
    main()
